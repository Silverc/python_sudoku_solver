### Possible changes: record the line and cube areas as much as possible to not have to look at the same thing multiple times
# 
#creates a buffer of the current board square
#def shorten_cube(board, pos):
#	cube = []
#	for i in range(int(pos[0]/3)*3,int(pos[0]/3)*3+3):
#		for j in range(int(pos[1]/3)*3,int(pos[1]/3)*3+3):
#			if(board[i][j] != 0):
#				cube.append(board[i][j])
#	return cube

#creates a buffer of the current board line
#def shorten_line(board, pos):
#	line = []
#	for i in range(0,N):
#		if(board[pos][i] != 0):
#			line.append(board[pos][i])
#	return line
#
###

N = 9

sudoku = [ [3, 0, 6, 5, 0, 8, 4, 0, 0], 
		 [5, 2, 0, 0, 0, 0, 0, 0, 0], 
		 [0, 8, 7, 0, 0, 0, 0, 3, 1], 
		 [0, 0, 3, 0, 1, 0, 0, 8, 0], 
		 [9, 0, 0, 8, 6, 3, 0, 0, 5], 
		 [0, 5, 0, 0, 9, 0, 6, 0, 0], 
		 [1, 3, 0, 0, 0, 0, 2, 5, 0], 
		 [0, 0, 0, 0, 0, 0, 0, 7, 4], 
		 [0, 0, 5, 2, 0, 6, 3, 0, 0] ]

def updatePos(x,y):
	if(y==N-1):
		x=x+1
	y=(y+1)%9
	return [x,y]

def check_column(board,x,current_try):
	for i in range(N):
		if(board[i][x] == current_try):
			return False
	return True

def check_line(board,y,current_try):
	for i in range(N):
		if(board[y][i] == current_try):
			return False
	return True

def check_cube(board,x,y,current_try):
	for i in range(int(x/3)*3,int(x/3)*3+3):
		for j in range(int(y/3)*3,int(y/3)*3+3):
			if(board[i][j] == current_try):
				return False
	return True

#tests if the num_try is in the checked array
def possible_test(array, num_try):
	for i in range(0,len(array)):
		if(array[i] == num_try):
			return False
	return True

def print_board(board):
	for i in range(N):
			for j in range(N):
				end_str = " "
				if((j+1)%3 == 0 and j<(N-1)):
					end_str = " | "
				print(board[i][j],end=end_str)
			if(i<N-1):
				print("\n"+"_"*(2*N+3))
	print("\n")

def solveTool(board,pos):
	#if the height of the searched space is higher than the board, the run was sucessfull
	if(pos[0]==N):
		return True

	line = []
	cube = []
	if(board[pos[0]][pos[1]]==0):
		for current_try in range(1,N+1):
			#checks if the current try works in this space
			if(check_cube(board,pos[0],pos[1],current_try) and 
			check_line(board,pos[0],current_try) and check_column(board,pos[1],current_try)):
				board[pos[0]][pos[1]] = current_try
				new_pos = updatePos(pos[0],pos[1])
				if(solveTool(board,new_pos)):
					return True
				#else:
				#	print(str(current_try)+" nao pode ser colocado")
			board[pos[0]][pos[1]] = 0
		return False            
	else:
		new_pos = updatePos(pos[0],pos[1])
		if(solveTool(board,new_pos)):
			return True
	


def solve(board):
	print("Original board: \n")
	print_board(board)
	if(solveTool(board,[0,0])):
		#print the board
		print("Solved board: \n")
		print_board(board)
	else:
		print("Unsolvable board")

def input_board():
	print("Input your board one line at a time, using 0's to represent empty spaces")
	print("Ex: 090103056")
	new_board = []
	for i in range(N):
		new_line = []
		user_input = input()
		for j in range(len(user_input)):
			new_line.append(int(user_input[j]))
		new_board.append(new_line)
	return new_board

def main():
	print("Choose what to do: \n")
	print("1: Input a board to solve\n")
	print("2: Solve a predetermined board\n")
	choice = input()
	if(choice == "1"):
		#call a function to input a board
		new_board = input_board()

		solve(new_board)
	if(choice == "2"):
		solve(sudoku)

def __init__():
	main()

__init__()


